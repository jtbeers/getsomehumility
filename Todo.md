

## Site Launch

* Article Page
    * _Remove swipe_
    * _add back category highlight_
    * _footer_
    * format bullets
    * sticky category
    * prev/next link at bottom
* Index page
    * _recent articles_
    * _Browse by category_
    * masonry?
* _Mobile design_
* 404 page
* make menu nicer


## Topic Ideas

* Humility in the life of Jesus
    * Washing disciples feet
    * Paying the temple tax
* Ask others about times that they were touched by humility in the life of someone else
* Moses and how he handed off authority to Joshua after a life of service
* Minoa and how he asked the Lord for wisdom on how to raise Samson
* Deborah and her challenge to the commander guy
* David and his refusal to harm Saul
* Bathsheba's husband, Uriah
* _Solomon's request for wisdom_
* _Judah and Tamar_
* Why is humility so hard?
* Responding to changes in your plan.
* Why do we often have the wrong definition of humility?
* Defining Humility
    * Humility
    * Fear of the Lord
    * Riches
    * Honor
    * Life
* Daniel and Nebuchadnezzer
* Joseph and how he was made 2nd in command
* Joseph and the way he forgave his brothers
* Joseph and how God progressively removed his pride - sold into slavery, falsely accused, forgotten
    * sold into slavery for his pride - judgement for his sinfulness
    * falsely accused - human sin condemns him despite doing the right thing
    * forgotten by the cupbearer - no longer prideful, acts righteously, pleads for help and is forgotten because its not God's timing
    * all this prepares him to say "Am I in the place of God?"
* Humility is figuring out what you want and why. In Pride we assume that we obviously want the best thing. 

