const { createFilePath } = require(`gatsby-source-filesystem`);
const path = require(`path`);

exports.onCreateNode = ({ node, getNode, actions }) => {
  const { createNodeField } = actions
  if (node.internal.type === `MarkdownRemark`) {
    const slug = createFilePath({ node, getNode, basePath: `pages` })
    createNodeField({
      node,
      name: `slug`,
      value: slug,
    })
  }
}

exports.createPages = async ({ graphql, actions }) => {
  await generateCategoryPages(graphql, actions);
}

async function generateCategoryPages(graphql, actions) {
  const { createPage } = actions
  const { data } = await graphql(`
    {
      allMarkdownRemark(filter: {frontmatter: {category: {ne: null}}}) {
        edges {
          node {
            fields {
              slug
            }
            frontmatter {
              title
              date
              category
              published
            }
            html
          }
        }
      }
  }
  `);

  const getSlug = ( node ) => {
    return node ? node.fields.slug.replace('/articleBits', '') : '';
  }

  data.allMarkdownRemark.edges.forEach(({ node }, i, arr ) => {
    const slug = node.fields.slug.replace( '/articles', '' );
    if( !node.frontmatter.published ){
      return;
    }
    createPage({
      path: getSlug( node ),
      component: path.resolve(`./src/layout/category.js`),
      context: {          // Data passed to context is available          // in page queries as GraphQL variables.
        slug: getSlug(node),
        frontmatter: node.frontmatter,
        article: {
          ...node,
          prev: getSlug( ( arr[ i - 1 ] || arr[ arr.length - 1 ] ).node ),
          next: getSlug( ( arr[ i + 1 ] || arr[ 0 ] ).node )
        }
      },
    });
  });
}