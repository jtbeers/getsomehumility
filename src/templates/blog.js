import React from "react"
import { graphql } from "gatsby"
import ContentLayout from  "../layout/content"
import styles from './blog.module.scss'

export default ({ data }) => {
	const post = data.markdownRemark;
	return (
      	<ContentLayout>
      		<div className={ styles.header }>
	      		<h1>{post.frontmatter.title}</h1>
	      	</div>
	      	<div
	      		dangerouslySetInnerHTML={{ __html: post.html }}
	      		className= { styles.blog_content }
	      	>
			</div>
		</ContentLayout>
  	)
}

export const query = graphql`
	query($slug: String!) {
		markdownRemark(fields: { slug: { eq: $slug } }) {
			html
			frontmatter {
				title
			}
		}
	}`