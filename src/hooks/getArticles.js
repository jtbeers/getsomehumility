import { useStaticQuery, graphql } from "gatsby"

const useCategoryArticles = () => {
    const data = useStaticQuery(
        graphql`
            query {
                articles: allSitePage(filter: {context: {frontmatter: {category: {ne: null}, published: {eq: true}}}}, sort: {fields: context___frontmatter___date, order: DESC}) {
                    edges {
                        node {
                            path
                            context {
                                frontmatter {
                                    published
                                    date
                                    category
                                    title
                                }
                                article {
                                    html
                                }
                            }
                        }
                    }
                }
            }
    `);

    return data.articles.edges.map(edge => edge.node);
}

const useArticlesByCategory = ( category ) => {
    return useCategoryArticles().filter( article => article.context.frontmatter.category === category );
}

export {
    useCategoryArticles,
    useArticlesByCategory
};