---
title: "Humility is... Thy power and Thine alone."
category: "riches"
date: "04-24-2020"
published: true
---

# What is Your Source?
<!-- readmore -->
Humility is often viewed as a fixation on our own weakness or insufficiency. True humility does indeed believe that we are weak and cannot accomplish what we need to. However, its ultimate focus is on the external power available to solve problems of epic proportions.
<!-- readmore -->
>Lord, now indeed I find, Thy power and Thine alone,
>can change the leper's spots, and melt the heart of stone.

These lyrics, from the hymn "Jesus Paid it All", are a wonderful summation of how Biblical humility gives us access to more power than we could ever hope to wield on our own. As humans, our natural prideful tendency is to latch on to any merit we think will give us an advantage over our neighbor. We cling to small achievements in order to stockpile evidence of our value, ready to convince anyone who might question us. This inevitably results in a life of insecurity. Humility's approach is different. Humility will freely admit its weakness but chooses instead to boast in the strength of others. When confronted with a problem pride says, "I can handle this". Humility says, "I can handle this with God's help."

# Power for the Lowly

Contrary to what so many people belive, humility - not pride, gets the job done. Humility has access to an incredible wealth of resources and power precisely because it doesn't try to be something it's not. Humility has access not only to its own resources and power but also those of others. And for those of us who love God and have submitted ourselves to Jesus Christ, humility grants us access even to divine power.

# pride's Cracked Foundation

Pride limits our ability to see real and lasting solutions to a problem. It assumes that it knows the right solution and has the ability to implement it. Pride cannot consider that there is a better alternative. The well known proverb "Pride commeth before the fall" (Proverbs 16:18) is more than just a familiar addage. There is so much truth wrapped up in those few words. Pride constantly undermines itself. When our identity is build on our own abilities we fail to see how the cracks in our foundation fracture their way through the whole. Eventually, the very thing we once took pride in becomes our downfall. The irony is that we can only really have pride when it is being propped up by something else.

True humility on the other hand can bring lasting solutions to a problem. No problem can be solved until it is fully understood. Humility cuts to the heart of the issue and admits that it does not fully understand the problem and therefore requires the strength and instruction of another. When we admit that our power is limited and can at best only solve the part of the problem that we understand then we are free to admit that we need another perspective to show us a fuller solution. Though humility requires us to put away our ego and admit that we cannot solve the problem on our own it always ultimately leads us to a solution far greater than we ever would have realized without it.

# The Lasting Solution

So what do we gain from putting aside our pride, growing in humility, and seeking the power of another? For one, we gain riches. As we humble ourselves and learn deeper solutions to our problems we come to realize that we are also gaining the resources to solve more and more problems in the future. With every solution comes experience and knowledge that will serve us for our entire lives. Humility, through accepting the help of others, gains riches that pride could never even know. So take the words of Jesus to heart "Abide in me, and I in you. As the branch cannot bear fruit by itself, unless it abides in the vine, neither can you, unless you abide in me." ([John 15:4](https://www.biblegateway.com/passage/?search=John+15%3A4&version=ESV)), combat the desire to bear your own fruit and enjoy humbly enjoy the rich fruit of abiding in the Vine.