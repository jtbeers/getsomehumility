---
title: "Humility is... Getting Rid of Assumptions"
category: "life"
date: "05-23-2020"
published: true
---

# One Problem and Ninety-Nine Solutions
<!-- readmore -->
Often, when presented with a problem, we assume we know the solution before we even fully understand what we are up against. Can you think of some examples? I can't find my keys! They must be in the... My kids are bored and need something to do! I will make them... A client at work is calling and complaining about some issue! We'll just tell them... While our reaction in the moment may stop the situation from getting worse or even appear to solve the problem, how do we know if it was the right answer? How often do we even ask that question?
<!-- readmore -->
Unfortunately, the answer is often, "not enough". Our knee jerk reaction is to throw anything we have at a problem to solve it without looking for a better option. When we neglect to look beyond our assumptions and consider a different perspective we miss opportunites and confine ourselves to our own limitations. These small, seemingly "harmless" assumptions are a sneaky form of Pride.

# A Quieter Alternative

One might protest and say, "Please, is it really prideful to think I can find my keys when they are lost?" Well, no, but also yes. If you are so capable of finding your keys, why did you lose them in the first place? I don't mean to reach, it's a silly example. It isn't prideful to assume you can accomplish _something_. People accomplish things all day everyday. It is prideful to assume that whatever random idea you came up with is _the solution_ to the probelm.

> Then Manoah prayed to the Lord: “Pardon your servant, Lord. I beg you to let the man of God you sent to us come again to teach us how to bring up the boy who is to be born.”

> [Judges 13:8](https://www.biblegateway.com/passage/?search=judges+13%3A8&version=ESV)

Manoah was a man tasked with difficult but common task. Raise a child. Sure, the kid he was tasked with raising came with some extra requirements but it wasn't like he was the only parent ever. Manoah could have just taken the promise of a child at face value and gone about his business, assuming he knew how to raise Samson. Manoah did not let his pride get the best of him and instead practically oozed humility as he asked to be taught the solution the responsibility that was being given to him. He took his charge seriously and did not assume to know how to be the father he should be. He asked to be taught what fatherhood should look like. Sure enough, the Angel of the Lord returned and a few verses later we see Manoah persist in his desire to understand what lays before him.

> So Manoah asked him, “When your words are fulfilled, what is to be the rule that governs the boy’s life and work?”

> [Judges 13:12](https://www.biblegateway.com/passage/?search=judges+13%3A12&version=ESV)

Manoah didn't just ask, "How can I get this done?" He wasn't only concerned with completing the task. He was concerned with doing it well. He didn't assume that his first instinct was sufficient. He asked specifc questions of the Lord in order to understand who his son would be and how to support Samson in fulfilling his destiny. Manoah was more concerned about faithfully solving the problem than he was about just crossing the finish line.

Manoah, even with divine guidance, was not so great a parent that Samson turned out perfectly. Samson wasn't even necessarily someone you would call a role model. Samson's life is riddled with a host of negative attributes. But what if Manoah hand't asked for guidance? What if he hadn't cared about learning how to raise a son with such a unique and intense gift as Samson? At the end of Samson's story, we see an arrogant strong man finally decide to humble himself. If Manoah hadn't modeled that for him, would his story have ended the same way?

# Pride Keeps Us From the Good Stuff

When we approach a problem pridefully it can often cloud our vision in different ways.

A common pitfall of pride is self-satisfaction. We are so enamored by our initial solution that we never even begin to look for something better. There may or may not be a better solution to your problem, but if you _assume_ you have the best solution you will never know. What if you took the time right now to think of a simple problem that you face and ask God how you could solve it better? Maybe it's asking about the way you carry grocerys out of the store. Maybe it's taking the time to consider how you want to approach your boss when discussing a certain topic. Could the answer be that you have nothing to improve? Certiainly! That would be a great thing to hear! Could the answer be a new perspective and new possibilities? Absolutely. Don't prevent yourself from finding success by being too content.

Fantsy is another exceptionally tricky way that pride holds us back. If it's true that you should never let success go to your head, how much more success that you don't actually have! Manoah didn't let daydreams of "Best Dad Ever" coffee mugs keep him from asking how to be a better dad. No, he kept his focus on the present and asked for tips on how to reach the fatherhood finish line.

Letting our assumptions determine how we solve problems is not the effective strategy we often think it is. When we abandon our assumptions and ask the Lord for better solutions it brings us the success we hope for. It frees us from the fear of finding out what happens when our pride finally catches up with us. Humility gives us victory far beyond our own limitations and leads us into a richer, fuller life. 










