---
title: "Humility is... being patient with other's faults."
category: "honor"
date: "12-16-2019"
published: false
---

Humility enables us to see others faults and respond with patience so that we can set a right example for them. Sometimes in our pride we would rather condemn people for their poor behavior and then demand that they act as we think they should. Often this is an unsuccessful approach. If we humble ourselves by recognizing our own imperfections and by valuing the other persons growth over our own "righteousness" we can respond to them with patience and encouragement that will pay off in the long run.

Humility enables us to see others faults and respond with patience so that we can set a right example for them. Sometimes in our pride we would rather condemn people for their poor behavior and then demand that they act as we think they should. Often this is an unsuccessful approach. If they really knew better why wouldn't they behave better? If we choose to humble ourselves and set a better example for others, even when their sinful brokeness make us furious, we are wisely playing the long game. Finally, when the seeds you've planted come to fruition the person will recognize their mistakes, see they way you treated them when you had ever reason to respond to them out of pride and anger, and they will honor the way that you treated them.


# Being a Dad is Great.

It is one of the greatest blessings I have ever received. It is a wonderful adventure that has new challenges around every corner. I love it and am thankful every day for the opportunity God has given me to raise some of His children. That being said, I do not like like applying rash cream with bare fingers (for the unitiated - parenting a child the first two years of their life is nearly equal parts preventing them from maiming themselves and rubbing rash cream on their butt crack). Another thing I don't like is the horrible way that kids sometimes treat each other. I am not averse to disciplining a child when they do something wrong but I was unprepared for the intense anger that wells up in me when one of my kids does something I deem _below_ them.

I'm not talking about refusing to eat a certain food, marking on the wall with crayons, or being to loud in the house. I'm talking about when my kids do a real injustice. Once, I broke up a fight between two of my kids. While the offender listened patiently and was genuinly affected, my other son took the opportunity to punch his brother in the face. Up until that point he had done nothing wrong, then in a moment of weakness, he let his anger get the best of him and socked his brother right in the nose. I was furious. So great was my rage that I am ashamed to say, for a brief moment, I gave up my mantle of "dad" and my son and I became _enemies_.

# Furious and Proud of it

It's not like I've never done anything wrong, or even for that matter, not hit someone. In the moment though, that didn't matter. My son had done something that I deemed so unjust that he should have known better. I had raised him better than that. He had had plenty of chances to learn his lesson and had evidently failed. The time for discipline was over, judgement had come. 

My fury boiled over and what followed was not a good example of how any parent should raise their kid. I'm sure that any lesson I hoped my child would learn was either drowned out by the angry yelling. If anything of value did get through it was surely marred by fear. It would have been so easy to walk away from that moment feeling justified - that even if I handled the situation wrong at least I was correcting some fault in my kids. I might not be a perfect dad but at least my kids wouldn't do that thing again. By the grace of God I was reminded of Jesus' humility and immediately my pride was shredded. 


notes ----
It can be hard to see others faults because we want good things for them. We want them to act better.

When dealing out "justice" we often forget or ignore our faults in favor of "that one horrible thing" they did.

Sometimes we think that we are better than someone else who is at fault. This is tricky because on one hand, we are better, we can
see the fault and we may even be in a position to administer justice. However, we can very easily fall into pride by looking at the other persons faults and deciding that their action was so heinous that it entitles us to respond however we wish.

There is only One that is good, It's not you and it's not me.

----------




This perspective, that we are enemies, is thoroughly wrong though. It's hypocritcal, self-serving, and prideful. I'd like to say that I struggle to recall a specific time that my parents caught me being a spiteful, cruel kid acting in malice towards someone else. But that would be a lie. I know for certain that there were plenty of times that I took advantage of someone weaker for my own personal gain.

The judgement I pronounce on my kids is self-serving as well. It often serves to validate my frustration and justify whatever response I deem appropriate. I worry about the harm I or others cause our children when we dispence heavy-handed angry justice and tell our kids it's "discipline".

It's also prideful because in that moment I forget that my children are each a gift, a blessing I received from God Himself. They are not here for my enjoyment (though I do enjoy them immensely) and they are not here to prop up my ego when they "act right". They were crafted by God in His image. I was charged with the sacred duty of wiping bottoms, dispensing snacks, enduring tantrums, and explaining discipline all for the good of those who in this moment I may deem my "enemy". It's important for me to recognize that when I am looking into another persons life and I get a glimpse of their ugliness - no matter how dark it is, I have been called to serve them patiently and lovingly, humbly, as my Savior did for me. This is not optional, its what I was made for and what God has called us each to do. 

The payoff is huge though. When we are patient with others in their faults it provides them a foothold to help them step out of their self-centeredness and grow into the man or woman God created them to be. It probably won't happen this very moment, today, or even anytime soon. But when it does happen, I believe there is a high probability that they will realize the patience we had with them, they will remember the kindness and encouragement they didn't deserve but that helped make them who they are today and they will appreciate it in a way that they probably won't even be able to express. As many times I can think of ways that I was caught acting like an idiot I can also think of a time that someone who cared about me overlooked my faults and continued to invest in me despite not seeing any returns. These moments have affected me in deep ways and led me to a deep sense of gratitude and respect, they are the most honorable people in the world to me.



# Topic
Choosing to be an ally instead of justifying your anger when someone makes a mistake allows you to sow into their lives in a way that will benefit them immensely and bring you honor in the future.

# Quesitons
__What is the central action?__
Putting aside your anger and choosing to work for the good of someone who is in the wrong.

__How does it show Biblical humility?__
God is slow to anger and patient. He is gentle and humble in heart.

__How does it bring wealth, honor, or life?__
When the person we are dealing with realizes that they were wrong and we were right they will also realize that we did not take advantage of it in the moment and instead pursued their good at personal cost.

__Whats wrong with just acting in anger?__
When we act out of anger we are automatically biased in the situation. We can not say we acted objectively for the other's good. We have to let whatever claim we feel we have to say "YOUR WRONG" and instead lovingly correct the other person in a way they may not deserve.

__Isn't this just letting them get off the hook?__
No, I'm not saying there is no opportunity to discipline or correct. I'm simply saying that there is a huge difference in disciplining someone "because they screwed up" and disciplining them to redeem them for their benefit.

# Outline

1. Thesis - Humility is choosing to walk with someone through their shortcomings instead of condemning them.
2. The problem - we often want to draw lines of "right" and "wrong"
    A. When I see my kids misbehave I often get very angry and want to force them to stop.
    B. This anger is ultimately rooted in pride and results in a fear based relationship with my kids.
3. How does humility solve this?
    A. Humility causes us to remember that we have often been on the "wrong" side
    B. Humility chooses to treat others for their good not necessarily how they deservce - The Lord does not deal wtih us as we deserve
    C. Humility opens up the opportunity for healing and not just punishment - The Lord takes away our sins
4. Conclusion
    A. Dealing humbly with others even when they are wrong shows that you are focused on healing, other focuesed, and have depth that many others do not
    B. When/If others realize what you have done for them they will also realize what it cost you and honor you.


# Draft



