---
title: "Humility is... not being too busy."
category: "life"
date: "11-24-2019"
published: false
---
I was reminded this week that business can itself be a form of pride. While frantically doing chores I caught myself thinking that I was the only one who ever did any work.

> "Why is the house always messy?"

> "Why do my coworkers need so much help all the time."

> "When is someone going to something that actually helps me instead of just asking me for things?"

It wasn't long after that the Lord helped me to take a break from my frantic self-centeredness and sat me down to rest. My messes remained but I gained peace stepping back from the things I thought needed to be accomplished.

As it turned out I didn't need to get all those things done, and I realized that I was actually not working very effectiently as my attitude prevented me from seeing how to solve the situation well.