---
title: "Humility is... admitting your issues run deep"
category: "life"
date: "01-24-2020"
published: true
---

# The Word "Systemic"

<!-- readmore -->

I was skeptical the first time I heard someone discuss they idea that some issue was "systemic". The issue they described seemed pretty isolated. I wasn't convinced that it was a big enough deal to warrant the "systemic" label. Even though I didn't agree with the labeling of their issue then, I do now. And not only that, I also think that are issues tend to me more systemic than not.

<!-- readmore -->

# What Does That Even Mean?

Colloquially, when people say that an issue is systemic they tend to mean that while the particular event in question happened at a specific time and place and for specific reasons its precursors can actually be traced back further to some ideologoy, perspective, or corporate fault. I often hear this word used to describe injustices performed against minorities or vulnerable peoples by a stronger group that ignorantly (and sometimes not) applies their power to the harm of others.

In short an issue is systemic if it has backstory.

# Know Thyself

For a good example of one man's obliviousness (as if they are hard to find!) look no further than the account of Genesis 38. In this story we are told about Judah, one of Jacob's twelve sons, and his complicated relationship with Tamar, his daughter-in-law. Judah had promised Tamar that she could marry his son, Shelah, in order to provide for Tamar. Judah had provided two other sons to her but both had died and one had seriously mistreated her. After his second son died, Judah decided that he didn't want to deal with the Tamar problem anymore and lied to her hoping that she would more or less dissappear. Eventaully it is discovered that Tamar has become pregnant as a result of prostitution. Judah is livid and demands that she be burned to death. As her final defense she reveals a staff and official seal she received months earlier as a pledge from the man that had inpregnated her. Tamar had made her point.

>Judah recognized them and said, “She is more righteous than I, since I wouldn’t give her to my son Shelah.” 
>Genesis 38:26

It wasn't until Tamar exposed Judah's sin that he came to the realize how deeply he had mistreated Tamar. Judah's admission was not just that he had in fact objectified her and approached her as a prostitute. He was confessing the way that he and his sons had systematically mistreated her throughout her life. Onan, Judah's second son, had essentially raped her instead of fulfilling his committment to provide for her. Judah had promised future provision for her through his son Shelah, but it was only a lie. And finally, Judah had called for her death the moment he perceived an opportunity to condemn her of her "sin".

Judah, humiliated, became aware of his faults and realized that they ran much deeper than he thought.

# Admit It

Often we minimize the deep roots our sins have. Sometimes, we are just oblivious to our own faults that we can't see the connection between our own crookedness  and the scandals it causes day in and day out. This is a horrible place to be. People can easily be caught in a cycle of negativity, completely unable to see the part of their personality that leads them astray over and over again. Somtimes, its so obvious to those around us that they can't imagine that we don't "know better".

Our only solution is to have some humility. It's painful to admit but our crookedness runs so deep that it often spills out in small ways despite our best efforts to seal up the cracks. When we minimize our faults and refuse to see the brokeness that lies behind the whitewashed walls we present to the outside world we are only protecting our pride and setting ourselves up for future failure. The quicker we humble ourselves, accept our sinfulness, our need for correction, and ask the Lord to provide. The quicker we will have a life overflowing with "streams of living water" (John 7:38) and will be given wisdom to mop up our messes by God who "gives generously to all without finding fault" (James 1:5).