# Where can I find this in scripture?

# Does this result in wealth, honor, or life?

# How does this show humility?

# In what ways do we commonly fail to walk out humility in this way?

# What is false humility's alternative to this?

# Outline

1. State the expression of humility
2. State the expected benefit
3. Detail the opposition
    A. How does Pride approach this situation...
    B. How does False Humility masquerade...
4. How does true Humility bring a solution to this problem.
5. How does Humility's solution bring wealth/honor/life?

