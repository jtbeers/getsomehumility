---
title: "Humility is... not taking God's spot."
category: "life"
date: "1-1-2020"
published: false
---

> But Joseph said to them, "Do not fear, for am I in the place of God? As for you, you meant evil against me, but God meant it for good, to bring it about that many people should be kept alibe, as they are today. So do not fear; I will provide for you and your little ones." Thus he comforted them and spoke kindly to them
> Genesis 50:19-21

When we take God's spot it always causes us to suffer. There are many ways that we can fall into this trap. We often have the head knowledge that we aren't God but often are actions reveal a deep pride that says, "well maybe in the situation I could be God."

Here we see that Joseph had an opportunity to dispense justice - every single piece of the puzzle was there. His brothers had committed a horrible crime against him when they sold him into slavery (as an alternative to murdering him). He, obiously, had witnessed it so he didn't need to prove their guilt. His brothers were openly acknowledging that they had wronged him and knew it was a severe enough injustice that they expected to be executed as a possible punishment. Yet, when Joseph responds to them the text says that "he comforted them and spoke kindly to them". That doesn't sound like the judgement I would expect the victim of their actions to hand out.

Had Joseph forgotten what they had done? Or maybe he remembered but had gone soft? No, we see in Genesis 50:19 as well as in Genesis 45:5, when Joseph first revealed his identity to his brothers, that Joseph considered their actions wrong and did not shy away from saying so. Why then would Joseph respond this way. 

Our answer is of course, Joseph's humility. Joseph's brothers had much to fear from him, and rightly so. His answer must have shocked them, "Do not fear, for am I in the place of God?" Joseph understood something that his brothers did not. He learned it when his father's favors was stripped from him and he was sold into slavery, he learned it when he was falsely accused by Potiphar's wife and thrown into jail. He learned it when he did Pharoah's cupbearer a favor and was then forgotten. God is the one who judges fairly and rightly, people are prone to misjudgement and self-centeredness. God is the one who decides what is best for us.

It's funny to me that Joseph had spoken this exact same comfort to his brothers 17 years earlier. Evidently they had not understood it. In Genesis 45:5 we read

> And now do not be dismayed or angry with yourselves because you sold me here, for God sent me before you to preserve life.

Joseph's brothers believed that man was the authority and that they were at the mercy of man whom they assumed they understood. Joseph believed that God was the one in authority, that His judgements were always good, and he respected them. This difference in faith caused Joseph's brothers to live in a fear based relationship with Joseph for nearly 2 decades. Joseph on the other hand had left all that behind and was living a life full of thankfulness and joy. It was precisely because of Joseph's humility that he was able to receive the good that God was doing even though it had come about through hard personal circumstances.


What are ways that you have tried to take God's place? What ways would it bring you life to grow in humility and let God sit where He is supposed to?