---
title: "Humility is... admitting you have no idea."
category: "riches"
date: "01-05-2020"
published: true
---

# An Imposter!

<!-- readmore -->

People are often very afraid of being "out of the loop." So much so that it seems like almost everyone has a story about a time that they experienced "imposter syndrome". The feeling that a certain success in your life is underserved and you accidentally became a fraudulent mastermind. I want to submit that sometimes the reason we feel like imposters is because we are. Truth be told few of us live up to the expectations that we or others place on ourselves. No amount of skill or ability will make you able to live up to another's expectations (let alone your own). We are all, in some ways, imposters. The real question is not "Am I an imposter". It's "I don't have the answers everyone thinks I do. What do I need to learn?"

<!-- readmore -->

That's what Solomon did, when God asked if He could do Solomon a favor. in 1 Kings 3, Solomon is visited by God and asked what he wants. He is essentially given a blank check already signed on the dotted line. Solomon's answer has a very subtle humility to it. He asks, "...Give your servant therefore an understanding mind to govern your people, that I may discern between good and evil, for who is able to govern this your great people?” (1 Kings 3:9). God is pleased with Solomon's request and agrees to grant him understanding as well as riches, honor, and long life (should Solomon follow God's precepts). Sidenote - riches, honor, life... that sounds suspiciously like something else I've heard in the Bible. Solomon's answer demonstrates his humility in a few wonderfuly subtle ways.

# Solomon's Answer

> And Solomon said, “You have shown great and steadfast love to your servant David my father, because he walked before you in faithfulness, in righteousness, and in uprightness of heart toward you. And you have kept for him this great and steadfast love and have given him a son to sit on his throne this day. And now, O Lord my God, you have made your servant king in place of David my father, although I am but a little child. I do not know how to go out or come in. And your servant is in the midst of your people whom you have chosen, a great people, too many to be numbered or counted for multitude. Give your servant therefore an understanding mind to govern your people, that I may discern between good and evil, for who is able to govern this your great people?”

> 1 Kings 3:6-9

Many times I've read this passage and glazed over the ways it demonstrates Solomon's humility. It's tempting to summarize Solomon's request as, "God, I have no idea what I'm doing. Please make me wise enough to be a good king." - but that is significantly different than what he asked. While the result of Solomon's request would lead to him being a good king he actually asked for the ability to serve his people well because he respected the Lord. Solomon recogonized three important things that each demonstrated his humility:

* his position was given to him by God
* that he is not fit for the task of ruling
* that being king means serving the people

God saw that Solomon wasn't just worried about maintaining his kingship. God saw that he wasn't just worried about his failure and embarrasment if he failed.

# Not an Imposter

Solomon wasn't worried about the consequences of being found out as an imposter. He didn't feel like an imposter *because he hadn't taken the kingship in the first place* he said it was *given* to him. The events that led to his succession took place before he was even born. The kingship granted to Solomon had nothing to do with his merit. So often when I hear others talk about imposter syndrome they express fear that they have manipulated the people around them and tricked them into giving them a job. Isn't that a strange perspective though? Doesn't that seem to be drenched in ego? "I am such an intellectual that I inadverntatly deceived an expert in their field." What!? That sounds like a very proud perspective to me. You are essentially saying that whoever evaluated you and hired you was so incompetent that they lacked the ability to even tell what they wanted let alone tell if you're skill set was of use to them. 

I think it is much better to assume that whoever gave you the position you are in is smarter than you are giving them credit for and that when they evaluated you for a given position they at least saw something worth taking a risk on.

# He Admitted His Lack

Solomon rightly discerned that he had not earned his position, but that it was given to him by the Lord. And if indeed the Lord had made him king, the Lord would also enable him to faithfully administer his charge. If Solomon needed God to provide ability for him to rule, that means that Solomon was admitting that he wasn't up to the task on his own. Note though, Solomon didn't say he couldn't do it. He said that he could do it if the Lord granted him understanding. He was capable of ruling a great people on the condition that God provided him strength. Whatever position you find yourself in, stop and ask the Lord for understanding. He is invested in your success and when He provides you understanding you will be capable of managing the task in front of you.

I want to note at this point that though Solomon asked for wisdom, and was granted it, he did not do everything perfectly. He had many moral failings, was sometimes not humble, and at points made very bad choices. In the end though the wisdom that had been granted him paid off.
<!-- link the above side note to some other articles about Solomon's shortcomings and the conclusion of Ecclisiastes -->

# He Realized it Wasn't Just About Him

One final point about Solomon's request is that its ultimate goal was to elevate others. Solomon would personally benefit immensely from his request for wisdom but that isn't *why* he asked for it. This is spelled out for us in the verses immediately following Solomon's request.

> So God said to him, “Since you have asked for this and not for long life or wealth for yourself, nor have asked for the death of your enemies but for discernment in administering justice, I will do what you have asked.

> 1 Kings 3:11

Would God have granted Solomon's request if he had asked for wealth to bless his people? I think so - if that had been what the people needed. At that moment though, when God asked Solomon what he wanted, wealth was not the most useful thing. Solomon was young, the promise of long life would not have benefitted the Israelites. His enemies had already stirred up lots of trouble, continued bloodshed and instability was not something his country needed. Solomon asked for wisdom because it was the single greatest thing he could receive to improve the lives of those that depended on him.

# What Do You Want?

So imagine, if you will, that God asked you the same question, "What shall I do for you?" How would you answer? Will you ask to not feel like an imposter and thus validate your ego? Is that what will honor God the most in whatever capactiy He has placed you? Is that what will bring about the most good for those around you or those who depend on you? I encourage you to take a lesson from Solomon and think about your current position in life. What is the one thing that you need most to best serve the people around you and by doing so faithfully administer that which God has given to you? That is the type of prayer that He delights in.
