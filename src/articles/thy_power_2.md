
Lord, now indeed I find, Thy power and Thine alone,
can change the leper's spots, and melt the heart of stone.

Humility is... realizing that we are in need of God's power.

# Where can I find this in scripture?
John 15 - Jesus is the vine, apart from Him we can do nothing.

# Does this result in wealth, honor, or life?
Wealth. When we seek God and ask Him for His solutions we come into abundance that we could not have achieved on our own. If we havd greater faith in God's ability and more humility regarding our own power we could access much more of what God has for us.

# How does this show humility?
To admit that we need God's help reveals that we are not in control of our own circumstance. It acknowledges God's power and sovreignty.

It also prevents us from falling into a "savior" mentality. Often we elevate ourselves by assuming that we have the solution to someone else's problem. When we confess that only God has the power we remove ourselves from the equation and enable others to receive what they need from the only one who can provide.

# In what ways do we commonly fail to walk out humility in this way?
We ignore God, instead choosing to focus on "completing" the task.
We pray and ask Him for help but fail to take the time to really bring it to Him as if we really needed His help. We only pay Him lip service.

# What is false humility's alternative to this?
False humility says I'm a worm, I can do nothing, I am useless, please do this for me.

------

<!-- 1. State the expression of humility -->
Humility is often viewed as a fixation on our own weakness or insufficiency. True Humility does indeed believe that we are weak and cannot accomplish what we need to. However, its ultimate focus is on the external power available to solve problems of epic proportions.

<!-- 2. State the expected benefit -->
>Lord, now indeed I find, Thy power and Thine alone,
>can change the leper's spots, and melt the heart of stone.

These lyrics, from the hymn "Jesus Paid it All", are a wonderful summation of how Biblical humility gives us access to more power than we could ever hope to wield on our own. As humans, our natural prideful tendency is to latch on to any merit we think will give us an advantage over our neighbor. We cling to small achievements in order to stockpile evidence of our value, ready to convince anyone who might question us. This inevitably results in a life of insecurity. Humility's approach is different. Humility will freely admit its weakness but chooses instead to boast in the strength of others. When confronted with a problem Pride says, "I can handle this". Humility says, "I can handle this with God's help."

Contrary to what so many people belive, Humility - not Pride, gets the job done. Humility has access to an incredible wealth of resources and power precisely because it doesn't try to be something it's not. Humility has access not only to its own resources and power but also those of others. And for those of us who love God and have submitted ourselves to Jesus Christ, humility grants us access even to divine power.

<!-- 
3. Detail the opposition
    A. How does Pride approach this situation...
    B. How does False Humility masquerade...
-->

Pride limits our ability to see real and lasting solutions to a problem. It assumes that it knows the right solution and has the ability to implement it. Pride cannot consider that there is a better alternative. The well known proverb "Pride commeth before the fall" (Proverbs 16:18) is more than just a familiar addage. There is so much truth wrapped up in those few words. Pride constantly undermines itself. When our identity is build on our own abilities we fail to see how the cracks in our foundation fracture their way through the whole. Eventually, the very thing we once took pride in becomes our downfall. The irony is that we can only really have pride when it is being propped up by something else.

False Humility on th eother hand, simply leads to stagnation and failure. False humility tries to pawn itself off as true humility by clai

<!-- 4. How does true Humility bring a solution to this problem. -->

True Humility on the other hand can bring lasting solutions to a problem. No problem can be solved until it is fully understood. Humility cuts to the heart of the issue and admits that it does not fully understand the problem and therefore requires the strength and instruction of another. When we admit that our power is limited and can at best only solve the part of the problem that we understand then we are free to admit that we need another perspective to show us a fuller solution. Though Humility requires us to put away our ego and admit that we cannot solve the problem on our own it always ultimately leads us to a solution far greater than we ever would have realized without it.

<!-- 5. How does Humility's solution bring wealth/honor/life? -->
So what do we gain from putting aside our pride, growing in humility, and seeking the power of another? For one, we gain wealth. As we humble ourselves and learn deeper solutions to our problems we come to realize that we are also gaining the resources to solve more and more problems in the future. With every solution comes experience and knowledge that will serve us for our entire lives. Humility, through accepting the help of others, gains the wealth that Pride could never even know. So take the words of Jesus to heart "Abide in me, and I in you. As the branch cannot bear fruit by itself, unless it abides in the vine, neither can you, unless you abide in me." ([John 15:4](https://www.biblegateway.com/passage/?search=John+15%3A4&version=ESV)), combat the desire to bear your won fruit and gain the wealth of humbly abiding in the Vine.