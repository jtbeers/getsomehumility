import React from "react";
import { graphql, Link } from "gatsby";

export default ( { bio } ) => (
	<div className='w-100 w5-ns mh5-ns ba br4 light-silver grow shadow-hover' style={{ boxShadow: '5px 10px 10px 2px rgba( 0, 0, 0, .2 )' }}>
		<Link to={bio.node.context.slug} className='flex flex-column-ns items-center-ns relative link black'>
			<img className = 'mb3 w-50 w-100-ns br4 br--top' style={{filter: 'grayscale(100%)'}} src='https://picsum.photos/300/300/?random' ></img>
			<div className="absolute static-ns bottom-0 mb3 ml3 no-underline f2 self-start black" >
				{bio.node.context.frontmatter.name}
			</div>
			<div className="self-center pa3">
				{bio.node.context.frontmatter.excerpt}
			</div>
		</Link>
	</div>
)