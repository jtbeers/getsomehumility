import React, { useState } from "react"
import { Link } from "gatsby"
import posed from 'react-pose';

import Fade from './fade.js'


const LinkWrapper = posed.ul({
	visible: {
		opacity: 1,
		transition: { duration: 2000 }
	},
	hidden: {
		opacity: 0
	}
});

const links = [
	{ name: 'home', href: '/' },
	{ name: 'about', href: '/about' },
	{ name: 'contact', href: '/contact' },
];

export default () => {
	const [ isVisible, updateVisible ] = useState( false );
	React.useEffect( () => {
		setTimeout( () => updateVisible( true ), 300 );
	});

	return ( 
		<div className = 'w5 bg-black flex flex-column' style={{height: '100vh'}}>
			<Fade className = 'light-silver' pose={isVisible ? 'visible' : 'hidden' }>
				<h1 className="pl3">
					<Link className = 'link' to='/'>humility</Link><
				/h1>
			</Fade>
			<LinkWrapper pose={isVisible ? 'visible' : 'hidden' }>
				{ links.map( link => (<li className = 'mb2'><Link className = 'link light-silver' to={link.href}>{link.name}</Link></li> ) ) }
			</LinkWrapper>
		</div>
	)
}