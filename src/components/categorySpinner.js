import React, { useState, useEffect } from "react"
import styles from "./categorySpinner.module.scss"

import posed from 'react-pose';

const ItemWrapper = posed.div({
    1: {
        top: '0%',
        width: '80%',
        height: '100%',
        left: '0%',
    },
    2: {
        top: '0%',
        height: '50%',
        left: '80%',
        width: '20%'
    },
    3: {
        top: '50%',
        height: '50%',
        left: '80%',
        width: '20%'
    }
});

const detectCategory = () => /\/(\w+)\//.exec( window.location.href )[ 1 ];

const determineOrder = category => {
    switch (category) {
        case 'riches':
            return {
                riches: 1,
                honor: 2,
                life: 3
            };
        case 'honor':
            return {
                riches: 3,
                honor: 1,
                life: 2
            };
        case 'life':
            return {
                riches: 2,
                honor: 3,
                life: 1
            };
    }
}


export default () => {
    const { riches, honor, life } = determineOrder( detectCategory() );
    const [ richesState, setRichesState ] = useState( riches );
    const [ honorState, setHonorState ] = useState( honor );
    const [ lifeState, setLifeState ] = useState( life );

    const makeActive = category => {
        const { riches, honor, life } = determineOrder( category );

        setRichesState( riches );
        setHonorState( honor );
        setLifeState( life );
    }
   
    return (
        <div className={styles.wrapper} >
            <ItemWrapper className={styles.category + ' bg-green '} onClick={() => makeActive( 'riches' ) } pose={ richesState }>
                riches
            </ItemWrapper>
            <ItemWrapper className={styles.category + ' bg-red '} onClick={() => makeActive( 'honor' ) } pose={ honorState }>
                honor
            </ItemWrapper>
            <ItemWrapper className={styles.category + ' bg-blue '} onClick={() => makeActive( 'life' ) } pose={ lifeState }>
                life
            </ItemWrapper>
        </div>
    )
}