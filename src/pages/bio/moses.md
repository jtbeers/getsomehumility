---
name: "Moses"
sort: 0
title: "The most humble man in the world"
date: "2017-08-10"
excerpt: "Moses called himself the most humble man in the world"
---

# Moses

[//]: # (Moses was the most humble man alive because he allowed God to dictate the reward for his efforts and trusted God to vindicate him.)

Exodus 2
Numbers 20:11
Deuteronomy 3:21-29
Deuteronomy 34

>>> The sand kicked up and scraped across my face - the heat was nearly unbearable. I needed to find water for my sheep. I knew of a stream not far from here that would be just what we needed. We could drink soon.

Moses had a life full of success and frustration alike. He once labeled himself [the most humble man alive]. But doesn't that invalidate his claim? Humility isn't what we've been trained to think. It's not about self-whipping, it's not about low self-esteem - it's more about the right appreciation of who you are and who others are. A correct understanding of the way that things are.

>>> Despite the heat, I was happy. This place, these stinking sheep, they were so much better than the life I left behind. You'd be shocked to know that I traded royalty for herding livestock, You'd laugh to hear that I wouldn't trade back. But soon enough, I would leave the new life I had found in a foregin land and I would return the hell-hole I fled from. I had started a slave, grown to be a prince, fled a fugitive, and I would return a shepherd. Though I can't say I was entirely willing.

Moses knew that he had a destiny. Through a miraculous turn of events that spared him death as a child and the instruction from his parents that one day his people would be freed from slavery he knew that one day he would liberate them. This knowledge turned first into pride before the Lord refined it.

Sometime in Moses' adulthood, around 40, he found an Egyptian beating a Hebrew slave. His destiny whispered to him, "Here is your chance, become the liberator of your people! You are a prince of Egypt aren't you! Take what is rightfully yours!". Moses stepped up to the plate and struck down the Egyptian. It wasn't until the next day though that he realized his destiny would not come through his pride or anger. His actions had been discovered and when he attempted to rule over his Hebrew brothers as judge they rejected him. He fell from favor with Pharoah, he was forced to flee.

>>> I pleaded with the Lord not to send me back. I was not skilled in leadership nor in communication. Not only did I not fit the task that was required of me I had already tried and failed. And what is more the people that I was being sent to had rejected me. They had rejected me for this exact task! We argued, The Almighty and I, though I confess I had no hope of winning. I would leave the desert that had become my home, and return to the land that spat me out decades ago.



[the most humble man alive]: https://www.bible.com/bible/1/NUM.12.3.KJV