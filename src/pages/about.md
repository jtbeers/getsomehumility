---
title: "What is Humility?"
date: "2019-04-05"
excerpt: "Can we develop a definition of humility that is actually useful?"
---

<h2 class='header-block'>
    <span class="header-block__spacer"></span>
    <span class='header-block__text'>What? How? Why?</span>
    <span class="header-block__spacer"></span>
</h2>
Suppose you walk into a room of people and ask, “What is humility?” You are likely to be told that humility is a virtue, that it is associated with respectable “saintly” type people, and an all around good thing. Intrigued, the credentials of humility seem impressive (saintly! that sounds pretty good!), you may follow-up with “How can I become more humble?” What might the people say? Probably something along the lines of “Don’t talk about yourself.”, “Let everyone else get ahead of you.”, “Ignore comfort and whip your back 100 times a day while calling yourself a worm.” At this point you may feel slightly confused. Wasn’t humility presented as such a lofty attribute. It is a virtue after all! Why then, does the act of humbling one’s self sound so… awful? Perhaps, there is simply something you are not aware of, maybe the payout is fantastic! You ask one final question, eager to here of the riches that await those who have endured the torturous journey of self-debasement. You simply ask, “Why?”

Unfortunately, I think you will get few meaningful answers and almost nothing helpful. Some might respond with a vapid “Virtues are good in general. Afterall, that is why we call them virtues.” Others, of the generous sort, might say “Humility doesn’t benefit the humble so much as it benefits those around them.”. Finally, and maybe the most helpful, an honest “I have no idea.” At least you know not to waste your time beating yourself up for nothing - err, for virtue!

<h2 class='header-block'>
    <span class="header-block__spacer"></span>
    <span class='header-block__text'>What Does Scripture Say?</span>
    <span class="header-block__spacer"></span>
</h2>
You’ve left the room now, thinking “Humility may have some value to a saint. But I’m not sure I see its appeal for my life. If it comes along I’ll take it but otherwise - eh.” Miraculously you notice a book on a table near you and open it to somewhere near the middle.

> “Humility and the fear of the Lord bring wealth, honor, and life.”
> (Proverbs 22:4, NIV)		

Well, that actually sounds pretty good. Wealth! Honor! Life! That doesn’t fit the description we were given earlier at all! Why was humility so misunderstood? The truth is, humility is a powerful thing. If mistaken for self-hatred and applied incorrectly it can waste a lot of time and ruin your life. Alternatively, if we trade our usual understanding of humility for something more valuable the rewards are many and have universal appeal.

Earlier we asked three questions. After finding the book we now have a much better answer to the final question, “Why?” Maybe this book contains an answer to the second question, “How?”

> “Rather, in humility value others above yourselves, not looking to your own interests but each of you to the interests of the others.”
> (Philippians 2:3-4, NIV)

Pause for a moment and reread the bit in quotes. It’s very possible that this sounds almost exactly like the answers given by the imaginary people we spoke to earlier. “Humility is about thinking less of yourself, and letting others take what is yours! See! We told you!” However, what we find in Philippians differs from our earlier definition by a very subtle omission. It doesn’t say you should think less of yourself. It does say that you should “value others above yourself” but not that you devalue yourself.

You could think that you are an amazing example of the human species and a literal gift to those around you - and as long as you value others even more you would be following this explanation of humility. Is it possible for someone to value themselves and others in such a way? This is, in my experience, very unnatural for humans. We tend to compare ourselves in a way that creates a super/inferior dichotomy. This new perspective on humility suggests that maybe that dichotomy is false and that maybe this line of thinking is a trap that actually makes us more focused on ourselves.

This is all well and good. New evidence has come to light and it suggests that there is actually a lot to gain from humility. We see that it doesn’t require thinking that you are worthless but actually consists of of valuing others highly. All that remains of from our survey is the definition of humility. Humility is a virtue, something especially pious people exude, and something generally good. This definition is much easier to stomach according to what we have read in scripture. Clearly humility is a virtue, it’s about valuing others well! Clearly it’s good, look at it’s many benefits! But really, that definition is a little thin. Virtue, is a broad term that I sometimes find a bit naive. I have never met an official saint and I’m not sure the criteria actually has to do with humility. Lot’s of things are good, but that doesn’t mean I understand them. How then can we improve our definition of humility?
Proverbs 22:4 told us that one of the results of humility is life. Proverbs 16:18 tells us that “pride leads to destruction.” Life - destruction. Success - failure. I feel an analogy coming on.

<h2 class='header-block'>
    <span class="header-block__spacer"></span>
    <span class='header-block__text'>Let’s Play a Game</span>
    <span class="header-block__spacer"></span>
</h2>
Consider this. What if life is like a game? All games boil down to having a set of choices that may or may not move you closer to winning. Let’s say you’re playing chess. During the game you can choose from many different moves. Most of the time, you will move in ways that keep your opponent from capturing your pieces and in ways to attack your opponent. You might even take their queen or put them in check. If you always make choices to protect your pieces, capture your opponents, or put the king in check, you have a good shot at winning. But let’s say you adopt a different strategy with a different set of priorities. Maybe you don’t like chess pieces and you prefer to focus on having a clear board. So you always choose to sacrifice your pieces with no capture in return, never take your opponent’s pieces, and avoid the king. You would lose quickly every time.

The point of this illustration is that success, winning the game, depends exclusively on having correct priorities. The degree that your priorities line up with the rules of the game is directly related to your potential for winning. It’s the same in real life. The more you understand how reality works and the rules that we must all live by, the more success you will experience. Pride, is a classic example of disordered priorities, which result in “losing.” The antidote is humility.
What do We Know Now?

Humility is having the right set of priorities. You act humbly by valuing yourself, valuing others even more highly, and most importantly - fearing the Lord. And what does it profit us? [wealth](/wealth), [honor](/honor), and [life](/life).
